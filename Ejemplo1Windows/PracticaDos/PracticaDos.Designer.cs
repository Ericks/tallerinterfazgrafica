﻿namespace PracticaDos
{
    partial class PracticaDos
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCentigrados = new System.Windows.Forms.TextBox();
            this.txtFarenheit = new System.Windows.Forms.TextBox();
            this.btnConvertir = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ingrese Grados Centigrados:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ingrese grados Farenheit:";
            // 
            // txtCentigrados
            // 
            this.txtCentigrados.Location = new System.Drawing.Point(30, 61);
            this.txtCentigrados.Name = "txtCentigrados";
            this.txtCentigrados.Size = new System.Drawing.Size(138, 20);
            this.txtCentigrados.TabIndex = 2;
            this.txtCentigrados.Text = "0.00";
            this.txtCentigrados.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCentigrados_KeyPress);
            // 
            // txtFarenheit
            // 
            this.txtFarenheit.Location = new System.Drawing.Point(30, 139);
            this.txtFarenheit.Name = "txtFarenheit";
            this.txtFarenheit.Size = new System.Drawing.Size(138, 20);
            this.txtFarenheit.TabIndex = 3;
            this.txtFarenheit.Text = "32.00";
            this.txtFarenheit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFarenheit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFarenheit_KeyPress);
            // 
            // btnConvertir
            // 
            this.btnConvertir.Location = new System.Drawing.Point(30, 187);
            this.btnConvertir.Name = "btnConvertir";
            this.btnConvertir.Size = new System.Drawing.Size(75, 23);
            this.btnConvertir.TabIndex = 4;
            this.btnConvertir.Text = "Convertir";
            this.btnConvertir.UseVisualStyleBackColor = true;
            this.btnConvertir.Click += new System.EventHandler(this.btnConvertir_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 233);
            this.Controls.Add(this.btnConvertir);
            this.Controls.Add(this.txtFarenheit);
            this.Controls.Add(this.txtCentigrados);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Convertidor de temperaturas";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCentigrados;
        private System.Windows.Forms.TextBox txtFarenheit;
        private System.Windows.Forms.Button btnConvertir;
    }
}

