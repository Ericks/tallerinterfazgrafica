﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PracticaDos
{
    public partial class PracticaDos : Form
    {
        private TextBox vartemp = null;
        public PracticaDos()
        {
            InitializeComponent();
        }

        private void btnConvertir_Click(object sender, EventArgs e)
        {

            try
            {
                double grados;
                if (vartemp == txtCentigrados)
                {
                    grados = Convert.ToDouble(txtCentigrados.Text) * 1.8 + 32;
                    txtFarenheit.Text = string.Format("{0:F}", grados);
                }
                if (vartemp == txtFarenheit)
                {
                    grados = ((Convert.ToDouble(txtFarenheit.Text) - 32) / 18);
                    txtCentigrados.Text = string.Format("{0:F}", grados);
                }

            }
            catch (FormatException)
            {

                txtCentigrados.Text = "0.00";
                txtFarenheit.Text = "32.00";
            }

        }

        private void txtCentigrados_KeyPress(object sender, KeyPressEventArgs e)
        {
            vartemp = (TextBox)sender;
        }

        private void txtFarenheit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar < '0' || e.KeyChar > '9')
            {
                e.Handled = true;
            }
            vartemp = (TextBox)sender;
        }
    } 
}

